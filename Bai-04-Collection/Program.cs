﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai_04_Collection
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            //LearnList();
            LearnList2();
        }
        static void LearnList2()
        {
            List<int> ds = new List<int>();
            Random ran = new Random();
            for (int i = 0; i < 10; i++)
            {
                ds.Add(ran.Next(100));
            }
            Console.WriteLine("Các phần tử trong danh sách gốc là: ");
            foreach (var item in ds)
            {
                Console.Write(item + " ");
            }
            ds.Sort();
            Console.WriteLine();
            Console.WriteLine("Các phần tử sau khi sắp xếp là: ");
            foreach (var item in ds)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
            Console.WriteLine("Mời bạn nhập vào số muốn tìm: ");
            int k = int.Parse(Console.ReadLine());
            int kq = ds.IndexOf(k);
            if (kq < 0)
            {
                Console.WriteLine($"Không tìm thấy {k} trong danh sách !");
            }
            else
            {
                Console.WriteLine($"Tìm thấy {k} tại vị trí {kq}");
            }
        }
        static void LearnList()
        {
            List<string> ds = new List<string>();
            ds.Add("An");
            ds.Add("Bình");
            ds.Add("Giải");
            ds.Add("Thoát");
            ds.Insert(2, "Thương");
            ds.Remove("Thương");
            ds.RemoveAt(2);
            Console.WriteLine("Danh sách các phần tử trong List: ");
            for (int i = 0; i < ds.Count; i++)
            {
                string s = ds[i];
                Console.WriteLine(s);
            }
        }
    }
}
