﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caculator
{
    class Program
    {
        static void Main(string[] args)
        {
            int choose = 0;

            while (true)
            {
                Console.WriteLine("===========MAY TINH CAM TAY===========");
                Console.WriteLine("1. Cong hai so.");
                Console.WriteLine("2. Tru hai so.");
                Console.WriteLine("3. Nhan hai so.");
                Console.WriteLine("4. Chia hai so.");
                Console.WriteLine("5. Chia lay du .");
                Console.WriteLine("6. Tinh luy thua a^b.");
                Console.WriteLine("0. Thoat !");
                Console.WriteLine("=======================================");
                Console.WriteLine("Vui long chon : ");
                choose = Convert.ToInt32(Console.ReadLine());
                int a = 0, b = 0;
                switch (choose)
                {
                    case 1:
                        Console.WriteLine("Nhap vao so a: ");
                        a = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Nhap vao so b: ");
                        b = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine($"{a} + {b} = {a + b}");
                        break;
                    case 2:
                        Console.WriteLine("Nhap vao so a: ");
                        a = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Nhap vao so b: ");
                        b = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine($"{a} - {b} = {a - b}");
                        break;
                    case 3:
                        Console.WriteLine("Nhap vao so a: ");
                        a = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Nhap vao so b: ");
                        b = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine($"{a} x {b} = {a * b}");
                        break;
                    case 4:
                        Console.WriteLine("Nhap vao so a: ");
                        a = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Nhap vao so b: ");
                        b = Convert.ToInt32(Console.ReadLine());
                        if (b != 0)
                        {
                            Console.WriteLine($"{a} / {b} = {1.0*(a / b)}");
                        }
                        else
                        {
                            Console.WriteLine("Mau so phai khac khong !");
                        }
                        break;
                    case 5:
                        Console.WriteLine("Nhap vao so a: ");
                        a = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Nhap vao so b: ");
                        b = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine($"{a} % {b} = {a % b}");
                        break;
                    case 6:
                        Console.WriteLine("Nhap vao so a: ");
                        a = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Nhap vao so b: ");
                        b = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine($"{a} ^ {b} = {Math.Pow(a,b)}");
                        break;
                    case 0:
                        Console.WriteLine("TAM BIET !!!");
                        System.Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Vui long nhap tu 1 den 6 !");
                        break;
                }
            }
        }
    }
}
