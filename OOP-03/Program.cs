﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_03
{
    class Program
    {
        static void TestHinhHoc()
        {
            HinhHoc hh;
            hh = new HinhHoc();
            Console.WriteLine("Diện tích của hình học: " + hh.TinhDienTich());
            hh = new HinhTron();
            Console.WriteLine("Diện tích của hình tròn: "+ hh.TinhDienTich());
        }
        static void TestHinhChuNhatHinhVuong()
        {
            HinhChuNhat hcn = new HinhChuNhat(3, 10);
            Console.WriteLine("Chu Vi  = "+hcn.ChuVi());
            Console.WriteLine("Diện tích = "+hcn.DienTich());
            HinhChuNhat hv = new HinhVuong(4);
            Console.WriteLine("Chu vi hình vuông: " + hv.ChuVi());
            Console.WriteLine("Diện tích hình vuông: "+ hv.DienTich());
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            //TestHinhHoc();
            //TestHinhChuNhatHinhVuong();
            //TestInterface();
            TestHocAsVaIs();
            
        }
        static void TestHocAsVaIs()
        {
            //ILamViec teo = new SinhVien();
            //if (teo is SinhVien)
            //{
            //    Console.WriteLine("Tèo là sinh viên");
            //    SinhVien x = teo as SinhVien;
            //}
            List<ILamViec> ds = new List<ILamViec>();
            SinhVien teo = new SinhVien() { MaSV = "m1", TenSV = "chu" };
            ds.Add(teo);
            ds.Add(new SinhVien() { MaSV = "1", TenSV = "ch" });
            foreach (ILamViec item in ds)
            {
                if (item is SinhVien)
                {
                    SinhVien sv = item as SinhVien;
                    Console.WriteLine("==> Sinh viên : "+sv.TenSV);
                }
            }
        }
        static void TestInterface()
        {
            ILamViec teo = new SinhVien();
            Console.WriteLine(teo.LamViec("học lập trình c# !"));
        }
    }
}
