﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_03
{
    public class SinhVien : ILamViec
    {
        public string MaSV { get; set; }
        public string TenSV { get; set; }
        public string LamViec(string moTa)
        {
            string s = $"Sinh viên đang {moTa}";
            return s;

        }
    }
}
