﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_03
{
    public class HinhChuNhat
    {
        public int canhA { get; set; }
        public int canhB { get; set; }
        public HinhChuNhat(int canhA, int canhB)
        {
            this.canhA = canhA;
            this.canhB = canhB;
        }
        public virtual int DienTich()
        {
            return canhA * canhB;
        }
        public virtual int ChuVi()
        {
            return (canhA + canhB) * 2;
        }
    }
}
