﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions
{
    class Fibonacci
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n : ");
            int n = int.Parse(Console.ReadLine());
            Console.Write($"{n} số Fibonacci đầu tiên là : ");
            for (int i = 0; i < n; i++)
            {
                Console.Write(Fibo(i) + " ");
            }
            Console.WriteLine();
        }

        static int Fibo(int n)
        {
            int f0 = 0, f1 = 1, fn = 1;
            if (n < 0)
            {
                return -1;
            }
            else if (n == 0 || n == 1)
            {
                return n;
            }
            else
            {
                for (int i = 2; i < n; i++)
                {
                    f0 = f1;
                    f1 = fn;
                    fn = f0 + f1;
                }
            }
            return fn;
        }
    }
}
