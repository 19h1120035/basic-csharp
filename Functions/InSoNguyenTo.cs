﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions
{
    class InSoNguyenTo
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            Console.Write($"{n} so nguyen to dau tien la: \n");
            int count = 0; // dem tong so nguyen to
            int i = 2;
            while (count < n)
            {
                if (isPrimeNumber(i) == 1)
                {
                    Console.Write($"{i}  ");
                    count++;
                }
                i++;
            }
        }
        private static int isPrimeNumber(int n)
        {
            if (n < 2)
            {
                return 0;
            }
            // check so nguyen to khi n >= 2
            int squareRoot = (int)Math.Sqrt(n);
            for (int i = 2; i <= squareRoot; i++)
            {
                if (n % i == 0)
                {
                    return 0;
                }
            }
            return 1;
        }
    }
}
