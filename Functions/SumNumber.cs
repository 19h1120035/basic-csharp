﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions
{
    class SumNumber
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine($"Tổng các chữ số trong {n} là : {SumNum(n)}");
        }
        static int SumNum(int n)
        {
            int soTachRa = 0;
            int sum = 0;
            while (n != 0)
            {
                soTachRa = n % 10;
                sum += soTachRa;
                n /= 10;
            }
            return sum;
        }
    }
}
