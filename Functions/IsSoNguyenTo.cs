﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions
{
    class IsSoNguyenTo
    {
        static int  Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine($"{n} là số nguyên tố : {isSoNguyenTo(n)}");
            return 0;

        }

        static bool isSoNguyenTo(int n)
        {
            int count = 0;
            for (int i = 1; i <= n; i++)
            {
                if (n %i == 0)
                {
                    count++;
                }
            }
            return count == 2;
        }
    }
}
