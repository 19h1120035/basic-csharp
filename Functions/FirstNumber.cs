﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Functions
{
    class FirstNumber
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine($"Số đầu tiên của {n} là : {FirstNum(n)}");
        }
        static int FirstNum(int n)
        {
            int soTachRa = 0;
            while (n!=0)
            {
                soTachRa = n % 10;
                n /= 10;

            }
            return soTachRa;
        }
    }
}
