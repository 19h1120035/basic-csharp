﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATMApplication
{
    class ATMApplicationV1
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.Title = "ỨNG DỤNG RÚT TIỀN ATM";
            string password = "123456";
            long ballance = 1000000;
            bool isLogin = default;
            long amount = default;
            int choice = default;
            while (true)
            {
                Console.WriteLine("============MENU=============");
                Console.WriteLine("|1. Đăng nhập vào Tài Khoản |");
                Console.WriteLine("|2. Rút tiền.               |");
                Console.WriteLine("|3. Nạp tiền vào Tài Khoản  |");
                Console.WriteLine("|4. Kiểm tra số dư          |");
                Console.WriteLine("|0. Thoát chương trình      |");
                Console.WriteLine("=============================");
                Console.WriteLine("Xin mời bạn chọn: ");
                choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        if (isLogin)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                            Console.WriteLine("Bạn đang đăng nhập vào Tài Khoản");
                            Console.ForegroundColor = ConsoleColor.Black;
                        }
                        else
                        {
                            Console.WriteLine("Nhập mật khẩu của bạn:");
                            var userPassword = Console.ReadLine();
                            if (userPassword.CompareTo(password) == 0)
                            {
                                isLogin = true;
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("Bạn đã đăng nhập thành công !");
                                Console.ForegroundColor = ConsoleColor.Black;
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Mật khẩu không đúng !");
                                Console.ForegroundColor = ConsoleColor.Black;
                            }
                        }
                        break;
                    case 2:
                        if (isLogin)
                        {
                            bool isWithdrawable = true;
                            int option = default;
                            Console.WriteLine("Chọn số tiền cần rút: ");
                            Console.WriteLine("1. 100k");
                            Console.WriteLine("2. 200k");
                            Console.WriteLine("3. 500k");
                            Console.WriteLine("4. 1000k");
                            Console.WriteLine("5. 2000k");
                            Console.WriteLine("6. Số tiền khác");
                            option = int.Parse(Console.ReadLine());
                            switch (option)
                            {
                                case 1:
                                    amount = 100000;
                                    break;
                                case 2:
                                    amount = 200000;
                                    break;
                                case 3:
                                    amount = 500000;
                                    break;
                                case 4:
                                    amount = 1000000;
                                    break;
                                case 5:
                                    amount = 2000000;
                                    break;
                                case 6:
                                    Console.WriteLine("Nhập vào số tiền cần rút:");
                                    amount = int.Parse(Console.ReadLine());
                                    if (amount % 100000 != 0)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Vui lòng nhập số tiền là bội số của 100k !");
                                        Console.ForegroundColor = ConsoleColor.Black;
                                        amount = 0;
                                        isWithdrawable = false;
                                    }
                                    break;
                                default:
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Vui lòng chọn lại !!!");
                                    Console.ForegroundColor = ConsoleColor.Black;
                                    break;
                            }
                            if (isWithdrawable)
                            {
                                if (ballance > amount)
                                {
                                    ballance -= amount;
                                    Console.ForegroundColor = ConsoleColor.Blue;
                                    Console.WriteLine($"Bạn đã rút {amount}VNĐ");
                                    Console.WriteLine($"Số tiền hiện tại trong Tài Khoản là: {ballance}VNĐ");
                                    Console.ForegroundColor = ConsoleColor.Black;
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Số dư không đủ !");
                                    Console.ForegroundColor = ConsoleColor.Black;
                                }
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Vui lòng đăng nhập để thực hiện giao dịch !");
                            Console.ForegroundColor = ConsoleColor.Black;
                        }
                        break;
                    case 3:
                        if (isLogin)
                        {
                            Console.WriteLine("Nhập số tiền muốn nạp vào Tài Khoản: ");
                            amount = long.Parse(Console.ReadLine());
                            if (amount > 0)
                            {
                                if (amount % 100000 == 0)
                                {
                                    ballance += amount;
                                    amount = 0;
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.WriteLine("Nạp tiền vào Tài Khoản thành công !");
                                    Console.ForegroundColor = ConsoleColor.Black;
                                }
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Số tiền nạp vào phải là ước số của 100k !");
                                    Console.ForegroundColor = ConsoleColor.Black;
                                }

                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Số tiền nạp vào không hợp lệ ! Vui lòng kiểm tra lại.");
                                Console.ForegroundColor = ConsoleColor.Black;

                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Vui lòng đăng nhập để thực hiện giao dịch này!");
                            Console.ForegroundColor = ConsoleColor.Black;
                        }
                        break;
                    case 4:
                        if (isLogin)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine($"Số tiền trong Tài Khoản của bạn hiện tại là: {ballance}VNĐ");
                            Console.ForegroundColor = ConsoleColor.Black;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Vui lòng đăng nhập để thực hiện giao dịch này !");
                            Console.ForegroundColor = ConsoleColor.Black;
                        }
                        break;
                    case 0:
                        Console.WriteLine("CẢM ƠN QUÝ KHÁCH ĐÃ SỬ DỤNG!");
                        System.Environment.Exit(0);
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Vui lòng chọn đúng !");
                        Console.ForegroundColor = ConsoleColor.Black;
                        break;
                }
            }
        }
    }
}
