﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();
            //Dictionary<string, double> dic2 = new Dictionary<string, double>();
            Console.OutputEncoding = Encoding.UTF8;
            dic.Add(1, "Đào Văn Thương");
            dic.Add(    2, "Nguyễn Thị Hiếu");
            if (dic.ContainsKey(2) == false)
            {
                dic.Add(2, "Hồ Văn Đồ");
            }
            dic.Add(3, "Chu Chỉ Nhược");
            dic.Add(4, "Chu Văn An");
            // để duyệt toàn bộ dữ liệu trong dictionary ta làm như sau:
            foreach (KeyValuePair<int,string> item in dic)
            {
                Console.WriteLine($"Mã = {item.Key} , Tên = {item.Value} ");
            }
            dic.Remove(2);
            Console.WriteLine("Sau khi xóa phần tử có khóa chính là 2: ");
            foreach (KeyValuePair<int, string> item in dic)
            {
                Console.WriteLine($"Mã = {item.Key} , Tên = {item.Value}");
            }
            string value = dic[3];
            Console.WriteLine("Đối tượng có khóa chính = 3 là :" +value);
            List<string> dsGiaTri =  dic.Values.ToList();
            Console.WriteLine($"Các giá trị là : ");
            foreach (var v in dsGiaTri)
            {
                Console.WriteLine(v);
            }
            List<int> dsKey = dic.Keys.ToList();
            Console.WriteLine("Các khóa chính là :");
            foreach (var key in dsKey)
            {
                Console.WriteLine(key);
            }
        }
    }
}
