﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_01
{
    class Program
    {
        static void TestLop()
        {
            SinhVien sv1 = new SinhVien();
            SinhVien sv2 = new SinhVien(19112003, "đào văn thương");
            SinhVien sv3 = new SinhVien();
            sv3.MaSV = 20;
            sv3.FullName= "nguyễn thị hiếu";
            sv3.DateOfBirth= new DateTime(2015 / 1 / 1);
            sv3.XuatThongTin();
            sv1.FullName = "chu chỉ nhược";
            sv1.MaSV = 25;
            Console.WriteLine(sv1);
            Console.WriteLine(sv2);
        }
        static void TestParam()
        {
            TestParam test = new TestParam();
            Console.WriteLine(test.Sum(1, 2, 3, 4, 5, 6, 8));
            Console.WriteLine(test.Sum(1, 2, 3));
        }
        static void TestAliasVaGomRac()
        {
            KhachHang kh1 = new KhachHang() { Ma = 1, Ten = "chu", Phone = "297843232" };
            KhachHang kh2 = new KhachHang() { Ma = 2, Ten = "thuong", Phone = "423872342" };
            kh1 = kh2;
            Console.WriteLine($"Tên của KH1 là: {kh1.Ten}");
            // như vậy lúc này ô nhớ mà kh1 đang trỏ trước lúc gán kh1 = kh2 bị thu hồi và lúc 
            //này ô nhớ kh2 đang trỏ có thêm kh1 đang trỏ nữa
            //=> kh1 hoặc kh2 đổi thì cũng làm cho cả kh1 và kh2 đổi luôn 
            kh1.Ten = "obama";
            Console.WriteLine($"Tên của KH2 là: {kh2.Ten}");
            KhachHang an = new KhachHang() { Ma = 3, Ten = "an", Phone = "3845" };
            KhachHang binh = an.copy();
            binh.Ten = "Bình Bình ";    
            Console.WriteLine($"Tên của bình : {binh.Ten}");
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            //TestLop();
            //TestParam();
            TestAliasVaGomRac();
        }
    }
}
