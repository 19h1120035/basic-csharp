﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_01
{
    public class SinhVien
    {
        #region các biến lớp
        private int maSV;
        private string fullName;
        private DateTime dateOfBirth;
        #endregion
        #region các constructor
        public SinhVien()
        {
            this.maSV = 1;
            this.fullName = "Thương";
            this.dateOfBirth = new DateTime();
        }

        public SinhVien(int maSV, string fullName)
        {
            this.maSV = maSV;
            this.fullName = fullName;
        }
        #endregion
        #region Các properties
        public int MaSV
        {
            get
            {
                return this.maSV;
            }
            set
            {
                this.maSV = value;
            }
        }
        public string FullName
        {
            set
            {
                this.fullName = value;
            }
            get
            {
                return this.fullName;
            }
        }
        public DateTime DateOfBirth
        {
            get
            {
                return this.dateOfBirth;
            }
            set
            {
                this.dateOfBirth = value;
            }
        }
        #endregion
        #region Các phương thức
        public override string ToString()
        {
            return this.maSV + "\t" + this.fullName;
        }
        private bool KiemTraNamSinh()
        {
            return (DateTime.Now.Year - this.dateOfBirth.Year) >= 17;
        }
        public void XuatThongTin()
        {
            if (KiemTraNamSinh() == false)
            {
                Console.WriteLine("Năm sinh không hợp lệ !");
            }
            else
            {
                Console.WriteLine(ToString());
            }
        }
        #endregion
    }
}
