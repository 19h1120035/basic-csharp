﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_01
{
    public class TestParam
    {
        public int Sum(params int[] arr)
        {
            int s = 0;
            foreach (var item in arr)
            {
                s += item;
            }
            return s;

        }
    }
}
