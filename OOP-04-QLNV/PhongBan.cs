﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_04_QLNV
{
    public class PhongBan
    {
        private List<NhanVien> dsNV = new List<NhanVien>();
        public int MaPhongBan { get; set; }
        public string TenPhongBan { get; set; }
        public NhanVien TruongPhong { get; set; }
        public bool ThemNhanVien(NhanVien nv)
        {
            bool trungMaNV = false;
            foreach (NhanVien oldNV in dsNV)
            {
                if (oldNV.MaNV == nv.MaNV)
                {
                    trungMaNV = true;
                    break;
                }
            }
            if (trungMaNV == true)
                return false;
            nv.Phong = this;
            dsNV.Add(nv);
            return true;
        }
        public void XuatToanBoNhanVien()
        {
            Console.WriteLine("Mã\t Tên \t Chức Vụ \t Lương");
            foreach (NhanVien nv in dsNV)
            {
                Console.WriteLine(nv);
            }
        }
        public NhanVien TimNhanVien(int maNV)
        {
            foreach (NhanVien old in dsNV)
                if (old.MaNV == maNV)
                    return old;
            return null;
        }
        public bool XoaNhanVien(int maNV)
        {
            NhanVien nv = TimNhanVien(maNV);
            if (nv == null)
                return false;
            dsNV.Remove(nv);
            return true;
        }
        private int compare(NhanVien nv1, NhanVien nv2)
        {
            int soSanhTheoTen =  string.Compare(nv1.TenNV,nv2.TenNV,true);
            if (soSanhTheoTen == 0)
            {
                if (nv1.MaNV < nv2.MaNV)
                    return 1;
                if (nv1.MaNV > nv2.MaNV)
                    return -1;
                return 0;
            }
            return soSanhTheoTen;
        }
        public void SapXep()
        {
            dsNV.Sort(compare);
        }
        public long TongLuong()
        {
            long sum = 0;
            foreach (NhanVien nv in dsNV)
            {
                sum += nv.TinhLuong;
            }
            return sum;
        }
    }
}
