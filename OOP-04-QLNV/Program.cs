﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_04_QLNV
{
    class Program
    {
        static List<PhongBan> dsPB = new List<PhongBan>();
        static void TestQuanLyNhanVien()
        {
            PhongBan pNS = new PhongBan();
            pNS.MaPhongBan = 1;
            pNS.TenPhongBan = "Phòng Nhân Sự";
            dsPB.Add(pNS);

            NhanVien teo = new NhanVien();
            teo.MaNV = 1;
            teo.TenNV = "Tèo";
            teo.ChucVu = LoaiChucVu.TRUONG_PHONG;
            pNS.ThemNhanVien(teo);

            NhanVien ty = new NhanVien();
            ty.MaNV = 2;
            ty.TenNV = "Tý";
            ty.ChucVu = LoaiChucVu.NHAN_VIEN;
            pNS.ThemNhanVien(ty);

            NhanVien tu = new NhanVien();
            tu.MaNV = 3;
            tu.TenNV = "Tý";
            tu.ChucVu = LoaiChucVu.NHAN_VIEN;
            pNS.ThemNhanVien(tu);

            NhanVien met = new NhanVien();
            met.MaNV = 4;
            met.TenNV = "Tèo";
            met.ChucVu = LoaiChucVu.NHAN_VIEN;
            pNS.ThemNhanVien(met);

            PhongBan phongKT = new PhongBan();
            phongKT.MaPhongBan = 2;
            phongKT.TenPhongBan = "Phòng Kế Toán";
            dsPB.Add(phongKT);

            NhanVien bin = new NhanVien();
            bin.MaNV = 4;
            bin.TenNV = "Bin Bin";
            bin.ChucVu = LoaiChucVu.PHO_PHONG;
            phongKT.ThemNhanVien(bin);
            Console.WriteLine("DANH SÁCH NHÂN VIÊN TRONG CÔNG TY: ");
            foreach (PhongBan pb in dsPB)
            {
                Console.WriteLine(pb.TenPhongBan);
                pb.XuatToanBoNhanVien();
            }
            NhanVien old = phongKT.TimNhanVien(4);
            if (old != null)
            {
                old.TenNV = "Cu Bin";
            }
            Console.WriteLine("DANH SÁCH NHÂN VIÊN SAU KHI SỬA: ");
            foreach (PhongBan pb in dsPB)
            {
                Console.WriteLine(pb.TenPhongBan);
                pb.XuatToanBoNhanVien();
            }
            if (pNS.XoaNhanVien(12) == false)
            {
                Console.WriteLine("Không tìm thấy mã Nhân Viên để xóa !");
            }
            else
            {
                Console.WriteLine("Xóa Nhân Viên thành công !");
                Console.WriteLine("DANH SÁCH NHÂN VIÊN SAU KHI XÓA: ");
                foreach (PhongBan pb in dsPB)
                {
                    Console.WriteLine(pb.TenPhongBan);
                    pb.XuatToanBoNhanVien();
                }
            }
            Console.WriteLine("DANH SÁCH NHÂN VIÊN THUỘC PHÒNG NHÂN SỰ: ");
            pNS.XuatToanBoNhanVien();
            pNS.SapXep();
            Console.WriteLine("DANH SÁCH NHÂN VIÊN THUỘC PHÒNG NHÂN SỰ SAU KHI SẮP XẾP: ");
            pNS.XuatToanBoNhanVien();
            long sum = 0;
            foreach (PhongBan pb in dsPB)
            {
                sum += pb.TongLuong();
            }
            Console.WriteLine("Tổng lương của tất cả nhân viên trong 1 tháng : " +sum);
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            TestQuanLyNhanVien();
        }
    }
}
