﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_04_QLNV
{
    public enum LoaiChucVu
    {
        GIAM_DOC,
        TRUONG_PHONG,
        PHO_PHONG,
        NHAN_VIEN
    }
}
