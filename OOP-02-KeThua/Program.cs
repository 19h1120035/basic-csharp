﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_02_KeThua
{
    class Program
    {
        static void TestNhanVien()
        {
            NhanVienChinhThuc teo = new NhanVienChinhThuc();
            teo.Ma = 1;
            teo.Ten = "Cu Tèo";
            Console.WriteLine($"Lương của {teo.Ten} = ");
            teo.TinhLuong();
        }
        static void TestNhanVien2()
        {
            NhanVienChinhThuc teo = new NhanVienChinhThuc();
            int luong = teo.TinhLuong(20);
            Console.WriteLine($"Lương của Tèo = {luong}");
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            TestNhanVien2();
        }
    }
}
