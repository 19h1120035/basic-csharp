﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_02_KeThua
{
    public class NhanVien
    {
        public int Ma { get; set; }
        public string Ten { get; set; }
        public void TinhLuong()
        {
            Console.WriteLine("Đây là phương thức tính lương cho nhân viên ");
        }
        public virtual int TinhLuong(int ngayCong)
        {
            return ngayCong * 100;
        }
    }
}
