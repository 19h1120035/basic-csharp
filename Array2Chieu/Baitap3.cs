﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array2Chieu
{
    class Baitap3
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[50];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Nhập vào phần tử thứ {i + 1} : ");
                int c = int.Parse(Console.ReadLine());
                arr[i] = c;
            }
            Console.Write("Các phần tử trong mảng là: ");
            for (int j = 0; j < n; j++)
            {
                Console.Write(arr[j] + " ");
            }
            Console.WriteLine();
            Console.Write("Nhập vào số nguyên x: ");
            int x = int.Parse(Console.ReadLine());
            arr[n] = x;
            int count = 0;
            Console.Write("Số phần tử trong mảng khi thêm : ");
            for (int k = 0; k <=n; k++)
            {
                Console.Write(arr[k] +" ");
                if (arr[k] == x)
                {
                    count++;
                }
            }
            Console.WriteLine($"\nSố lần xuất hiện của phần tử x trong mảng : {count}");
        }
    }
}
