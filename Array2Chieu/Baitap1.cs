﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array2Chieu
{
    class Baitap1
    {
        // Tìm Min, Max của mảng
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[50];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Nhập vào phần tử thứ {i + 1} : ");
                int c = int.Parse(Console.ReadLine());
                arr[i] = c;
            }
            Console.WriteLine("Các phần tử trong mảng là: ");
            for (int j = 0; j < n; j++)
            {
                Console.WriteLine($"Arr[{j}] = {arr[j]}");
            }
            Console.WriteLine($"Giá trị lớn nhất trong mảng : {timMax(arr, n)}");
            Console.WriteLine($"Giá trị lớn nhất thứ hai trong mảng : {timMax2(arr, n)}");
            Console.WriteLine($"Giá trị nhỏ nhất trong mảng : {timMin(arr, n)}");
            Console.WriteLine($"Giá trị nhỏ nhất thứ hai trong mảng: {timMin2(arr,n)}");        }


        static int timMax(int[] arr, int n)
        {
            int max = arr[0];
            for (int i = 0; i < n; i++)
            {
                if (max < arr[i])
                {
                    max = arr[i];
                }
            }
            return max;
        }
        static int timMin(int[] arr, int n)
        {
            int min = arr[0];
            for (int i = 0; i < n; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                }
            }
            return min;
        }
        static int timMax2(int[] arr, int n)
        {
            int p = 0, max = 0, max2 = 0;
            for (int i = 0; i < n; i++)
            {
                if (max < arr[i])
                {
                    max = arr[i];
                    p = i;
                }
            }
            for (int j = 0; j < n; j++)
            {
                if (j == p)
                {
                    j++;  /* bo qua phan tu lon nhat */
                    j--;
                }
                else
                {
                    if (max2 < arr[j])
                    {
                        max2 = arr[j];
                    }
                }
            }
            return max2;
        }
        static int timMin2(int[] arr, int n)
        {
            int p = 0, min = arr[0], min2 = timMax(arr, n);
            for (int i = 0; i < n; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                    p = i;
                }
            }
            for (int j = 0; j < n; j++)
            {
                if (j == p)
                {
                    j++;  /* bo qua phan tu lon nhat */
                    j--;
                }
                else
                {
                    if (min2 > arr[j])
                    {
                        min2 = arr[j];
                    }
                }
            }
            return min2;
        }
    }
}

