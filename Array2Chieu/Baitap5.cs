﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array2Chieu
{
    class Baitap5
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[50];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Nhập vào phần tử thứ {i + 1} : ");
                int c = int.Parse(Console.ReadLine());
                arr[i] = c;
            }
            Console.Write("Các phần tử trong mảng là: ");
            string x = "";
            for (int j = 0; j < n; j++)
            {
                Console.Write(arr[j] + " ");
                if (arr[j] == arr[n-j-1])
                {
                    x = "Là mảng đối xứng !";
                }
                else
                {
                    x = "Không phải mảng đối xứng !";
                }
            }
            Console.WriteLine();
            Console.WriteLine(x);

        }
    }
}
