﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array2Chieu
{
    class Baitap4
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[50];
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"Nhập vào phần tử thứ {i + 1} : ");
                int c = int.Parse(Console.ReadLine());
                arr[i] = c;
            }
            Console.Write("Các phần tử trong mảng là: ");
            for (int j = 0; j < n; j++)
            {
                Console.Write(arr[j] + " ");
            }
            Console.WriteLine();
            double tbcSoChan = 0;
            double tbcSoLe = 0;
            int countChan = 0, countLe = 0;
            for (int k = 0; k < n; k++)
            {
                if (arr[k] %2 == 0)
                {
                    tbcSoChan += arr[k];
                    countChan++;
                }
                else
                {
                    tbcSoLe += arr[k];
                    countLe++;
                }
            }
            Console.WriteLine($"Trung bình cộng các số chẵn trong mảng: {tbcSoChan} / {countChan} = {1.0*tbcSoChan/countChan}");
            Console.WriteLine($"Trung bình cộng các số lẻ trong mảng: {tbcSoLe} / {countLe} = {1.0*tbcSoLe/countLe}");
        }
    }
}
