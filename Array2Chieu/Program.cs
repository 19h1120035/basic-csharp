﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array2Chieu
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            //Mảng hai chiều
            // Mảng vuông
            int[,] matrix = new int[10, 8];
            bool[,] cons = new bool[5, 5];
            string[,] somthing = new string[6, 8];

            int row = matrix.GetLength(0); // số hàng
            int col = matrix.GetLength(1); // số cột

            Console.WriteLine($"Số hàng: {row}, số cột: {col}");
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = i * j;
                    Console.Write(matrix[i, j] + " ");

                }
                Console.WriteLine();
            }
            // Mảng zigzag
            int[][] matrix2 = new int[10][];
            for (int i = 0; i < matrix2.Length; i++)
            {
                matrix2[i] = new int[i + 1];
            }
            row = matrix2.Length; // lấy số hàng
            int colOfRow8 = matrix2[8].Length; // lấy số cột của hàng thứu nhất
            Console.WriteLine($"Số hàng: {row} , số cột: {colOfRow8}");
            for (int i = 0; i < matrix2.Length; i++)
            {
                for (int j = 1; j < matrix2[i].Length; j++)
                {
                    matrix2[i][j] = j - i;
                    Console.Write(matrix2[i][j] + " ");
                }
                Console.WriteLine();
            }

        }
    }
}
