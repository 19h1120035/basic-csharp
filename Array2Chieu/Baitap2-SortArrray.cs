﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array2Chieu
{
    class Baitap2_SortArrray
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Nhập vào số nguyên n: ");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine($"Nhập vào phần tử thứ {i + 1} : ");
                int c = int.Parse(Console.ReadLine());
                arr[i] = c;
            }
            Console.WriteLine("Các phần tử trong mảng là: ");
            for (int j = 0; j < arr.Length; j++)
            {
                Console.WriteLine($"Arr[{j}] = {arr[j]}");
            }
            Array.Sort(arr);
            Console.WriteLine("Mảng sau khi sắp xếp tăng dần: ");
            for (int f = 0; f < arr.Length; f++)
            {
                Console.Write(arr[f] + " ");
            }
            Console.WriteLine();
            Array.Reverse(arr);
            Console.WriteLine("Mảng sau khi sắp xếp giảm dần: ");
            for (int k = 0; k < arr.Length; k++)
            {
                Console.Write(arr[k] + " ");
            }
            Console.WriteLine();
        }
    }
}
