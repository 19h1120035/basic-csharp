﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TraTuDien
{
    class Program
    {
        static Dictionary<string, string> dic = new Dictionary<string, string>();
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            while (true)
            {
                menu();
                Console.WriteLine("Bạn có tiếp tục sử dụng tra từ điển không (c/k) : ");
                string s = Console.ReadLine();
                if (s == "k")
                {
                    break;
                }
            }
            Console.WriteLine("BYE BYE !!!");
        }
        static void menu()
        {
            Console.WriteLine("======================");
            Console.WriteLine("1. Tạo từ điển mới.   ");
            Console.WriteLine("2. Sủa từ điển.       ");
            Console.WriteLine("3. Tra cứu từ điển.   ");
            Console.WriteLine("4. Xóa từ điển.       ");
            Console.WriteLine("======================");
            Console.WriteLine("Chọn chức năng: ");
            try
            {
                int choose = int.Parse(Console.ReadLine());
                switch (choose)
                {
                    case 1:
                        TaoTuMoi();
                        break;
                    case 2:
                        SuaTu();
                        break;
                    case 3:
                        TraCuu();
                        break;
                    case 4:
                        XoaTu();
                        break;
                    default:
                        Console.WriteLine("Vui lòng chọn đúng chức năng trên !!");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Gặp lỗi rồi " + ex.Message);
            }
        }

        private static void XoaTu()
        {
            Console.WriteLine("Mời bạn nhập vào từ muốn xóa: ");
            string tiengAnh = Console.ReadLine();
            if (dic.ContainsKey(tiengAnh))
            {
                dic.Remove(tiengAnh);
                Console.WriteLine($"Xóa thành công từ {tiengAnh}");
            }
            else
            {
                Console.WriteLine($"Không tìm thấy từ {tiengAnh} để xóa !");
            }
        }

        private static void TraCuu()
        {
            Console.WriteLine("Mời bạn nhập vào từ Tiếng Anh muốn tra cứu : ");
            string tiengAnh = Console.ReadLine();
            if (dic.ContainsKey(tiengAnh))
            {
                string tiengViet = dic[tiengAnh];
                Console.WriteLine($"Nghĩa của {tiengAnh} là : {tiengViet}");

            }
            else
            {
                Console.WriteLine($"Từ điển chưa có cập nhập từ {tiengAnh}");
            }
        }

        private static void SuaTu()
        {
            Console.WriteLine("Nhập vào từ Tiếng Anh để sửa lại nghĩa: ");
            string tiengAnh = Console.ReadLine();
            if (dic.ContainsKey(tiengAnh) == false)
            {
                Console.WriteLine($"Không tìm thấy {tiengAnh} để sửa !");
            }
            else
            {
                Console.WriteLine("Mời bạn nhập lại nghĩa tiếng Việt: ");
                string tiengViet = Console.ReadLine();
                dic[tiengAnh] = tiengViet;
            }
        }

        private static void TaoTuMoi()
        {
            Console.WriteLine("Mời bạn nhập vào từ Tiếng Anh : ");
            string tiengAnh = Console.ReadLine();
            if (dic.ContainsKey(tiengAnh.ToLower()))
            {
                Console.WriteLine($"Từ {tiengAnh} đã tồn tại rồi !");
            }
            else
            {
                Console.WriteLine("Mời bạn nhập vào nghĩa tiếng Việt : ");
                string tiengViet = Console.ReadLine();
                dic.Add(tiengAnh, tiengViet);
            }
        }
    }
}

