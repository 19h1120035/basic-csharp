﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai_03
{
    class XuLyChuoi
    {
        // 1. Đếm xem chuỗi có bao nhiêu in hoa, in thường, số
        // 2. Đếm số khoảng trắng trong chuỗi
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            //XuLyChuoi1();
            //XuLyChuoi2();
            //XuLyChuoi3();
            //XuLyChuoi4();
            //XuLyChuoi5();
            //XuLyChuoi6();
            //XuLyChuoi7();
            ToiUuChuoi();
        }
        // Cho 1 chuỗi gốc, yêu cầu:
        // Xóa các khoảng trắng dư thừa bên trái, phải, các từ
        // Cách nhau bởi 1 khoảng trắng, kí tự đầu tiên viết hoa
        static void ToiUuChuoi()
        {
            Console.WriteLine("Mời bạn nhập vào 1 tên: ");
            string fullName = Console.ReadLine();
            Console.WriteLine($"Chuỗi gốc bạn vừa nhập: {fullName}");
            fullName = fullName.Trim();
            string[] arr = fullName.Split(new char[] { ' ' },
                StringSplitOptions.RemoveEmptyEntries);
            string newName = "";
            for (int i = 0; i < arr.Length; i++)
            {
                string word = arr[i];
                word = word.ToLower();
                char[] arrWord = word.ToCharArray();
                arrWord[0] = Char.ToUpper(arrWord[0]);
                string newWord = new string(arrWord);

                newName += newWord + " ";
            }
            newName = newName.Trim();
            Console.WriteLine($"Chuỗi sau khi tối ưu: {newName}");


        }
        static void XuLyChuoi7()
        {
            string info = "19H1120035;Đào Văn Thương;CN19CLCA";
            string[] arr = info.Split(';');
            Console.WriteLine($"Mã sinh viên: {arr[0]}");
            Console.WriteLine($"Họ và tên sinh viên: {arr[1]}");
            Console.WriteLine($"Lớp: {arr[2]}");
        }
        static void XuLyChuoi6()
        {
            string s = "D:/baihat/nhactrinh/em của ngày hôm qua.mp3";
            // lấy tên bài hát
            int vtCuoi = s.LastIndexOf("/");
            string sing = s.Substring(vtCuoi + 1);
            int vt2 = sing.LastIndexOf(".");
            string tenBaiHat = sing.Substring(0, vt2);


            Console.WriteLine($"Tên bài hát: {tenBaiHat}");
        }
        static void XuLyChuoi5()
        {
            string s = "D:/baihat/nhactrinh/anh yeu em.mp3";
            // tìm vị trí đầu tiên của kí tự /
            int vtDau = s.IndexOf("/");
            Console.WriteLine($"kí tự / ở vị trí thứ {vtDau}");
            // tìm vị trí cuối của kí tự /
            int vtCuoi = s.LastIndexOf("/");
            Console.WriteLine($"kí tự / ở vị trí thứ {vtCuoi}");
        }
        static void XuLyChuoi1()
        {
            Console.WriteLine("Mời bạn nhập vào 1 chuỗi: ");
            string s = Console.ReadLine();
            int countUpper = 0, countLower = 0, countDigit = 0, countSpace = 0;
            char[] arr = s.ToCharArray();
            for (int i = 0; i < arr.Length; i++)
            {
                if (Char.IsLower(arr[i]))
                    countLower++;
                if (Char.IsUpper(arr[i]))
                    countUpper++;
                if (Char.IsDigit(arr[i]))
                    countDigit++;
                if (Char.IsWhiteSpace(arr[i]))
                    countSpace++;
            }
            Console.WriteLine($"Có {countLower} kí tự in thường !");
            Console.WriteLine($"Có {countUpper} kí tự in hoa !");
            Console.WriteLine($"Có {countDigit} kí tự số !");
            Console.WriteLine($"Có {countSpace} kí tự khoảng trắng !");
            Console.WriteLine();
        }
        static void XuLyChuoi2()
        {
            Console.WriteLine("Mời bạn nhập vào 1 chuỗi: ");
            string s = Console.ReadLine();
            int countUpper = 0, countLower = 0, countDigit = 0, countSpace = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (Char.IsLower(s[i]))
                    countLower++;
                if (Char.IsUpper(s[i]))
                    countUpper++;
                if (Char.IsDigit(s[i]))
                    countDigit++;
                if (Char.IsWhiteSpace(s[i]))
                    countSpace++;
            }
            Console.WriteLine($"Có {countLower} kí tự in thường !");
            Console.WriteLine($"Có {countUpper} kí tự in hoa !");
            Console.WriteLine($"Có {countDigit} kí tự số !");
            Console.WriteLine($"Có {countSpace} kí tự khoảng trắng !");
            Console.WriteLine();
        }
        static void XuLyChuoi3()
        {
            Console.WriteLine("Mời bạn nhập vào 1 chuỗi: ");
            string s = Console.ReadLine();
            Console.WriteLine("Mời bạn nhập chuỗi tiếp theo: ");
            string s2 = Console.ReadLine();
            int result = s.ToLower().CompareTo(s2.ToLower());
            if (result == 0)
            {
                Console.WriteLine($"chuỗi ({s}) và ({s2}) giống nhau ");
            }
            else if (result < 0)
            {
                Console.WriteLine("s < s2");
            }
            else
            {
                Console.WriteLine("s > s2");
            }

        }
        static void XuLyChuoi4()
        {
            string s = string.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Now);
            Console.WriteLine(s);

        }
    }
}
