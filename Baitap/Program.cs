﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTDoanSo
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Play();
                Console.WriteLine("Chơi nữa không : ");
                string s = Console.ReadLine();
                if (s.ToLower() == "k")
                {
                    break;
                }
                Console.WriteLine();
            }
        }
        static void Play()
        {
            Random ran = new Random();
            int soCuaMay = ran.Next(501);
            int soCuaNguoi;
            int soLanDoan = 0;
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Máy đã ra 1 số trong [0...500], mời bạn đoán: ");
            while (true)
            {
                soCuaNguoi = int.Parse(Console.ReadLine());
                soLanDoan++;
                Console.WriteLine($"Bạn đoán lần thứ {soLanDoan}");
                if (soCuaNguoi == soCuaMay) // ngừng, vì đoán trúng
                {
                    Console.WriteLine($"Chúc mừng bạn đã đoán đúng, số của máy = {soCuaMay}");
                    Console.WriteLine();
                    break;
                }
                if (soCuaNguoi < soCuaMay)
                {
                    Console.WriteLine("Số bạn đoán nhỏ hơn số của máy !");
                }
                else
                {
                    Console.WriteLine("Số bạn đoán lớn hơn số của máy !");
                }
                if (soLanDoan == 7)
                {
                    Console.WriteLine("GAME OVER ! Bạn đã đoán quá 7 lần .");
                    Console.WriteLine($"Số của máy  = {soCuaMay}");
                    break;
                }
            }

        }
    }
}
