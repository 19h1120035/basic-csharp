﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai_02
{
    class Ex01
    {

        static void Main(string[] args)
        {
            // vd1: viết chương trình in ra nội dung nhập từ bàn phím
            /*
            Console.WriteLine("Vui long nhap vao 1 chuoi: ");
            string msg = Console.ReadLine();
            Console.WriteLine($"Chuoi ban vua nhap la: {msg}");
            */

            //vd2: kiểm tra chẵn lẻ số nhập vào, có chia hết cho 3 kh, in kq ra màng hình 
            /*
            Console.WriteLine("Nhap vao so nguyen n: ");
            int n = Convert.ToInt32(Console.ReadLine());
            if (n % 3 == 0)
            {
                Console.WriteLine($"{n} la cho chan !");
                Console.WriteLine($"{n} / 3 = {n/3}");
            }
            else
            {
                double a = (double)n / 3;
                Console.WriteLine($"{n} la cho lẻ !\nn = {a}");
            }
            */

            //vd3: thực hiện phép tính 
            /*
            Console.WriteLine("Nhap vao so nguyen a: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap vao so nguyen b: ");
            int b = Convert.ToInt32(Console.ReadLine());
            if (b != 0)
            {
                double kq = (double)a / b;
                Console.WriteLine($"Phep cong: {a} + {b} = {a + b}");
                Console.WriteLine($"Phep tru: {a} - {b} = {a - b}");
                Console.WriteLine($"Phep nhan: {a} x {b} = {a * b}");
                Console.WriteLine($"Phep chia: {a} / {b} = {kq}");
                Console.WriteLine($"Phep chia lay du: {a} % {b} = {(double)a%b}");

            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("So b phai lon hon 0 !");
                Console.ForegroundColor = ConsoleColor.Black;

            }
            */

            // vd4: nhập hai số a và b, so sánh và in ra
            /*
            Console.WriteLine("Nhap so nguyen a: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen b: ");
            int b = Convert.ToInt32(Console.ReadLine());
            if (a > b)
            {
                Console.WriteLine($"{a} > {b}");
            }
            else if (a < b)
            {
                Console.WriteLine($"{a} < {b}");
            }
            else
            {
                Console.WriteLine($"{a} = {b}");
            }
            */

            //vd5: nhập 4 số nguyên, tìm min,
            /*
            Console.WriteLine("Nhap so nguyen a: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen b: ");
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen c: ");
            int c = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen d: ");
            int d = Convert.ToInt32(Console.ReadLine());
            int min1 = Math.Min (a, b);
            int min2 = Math.Min (min1, c);
            int min3 = Math.Min(min2, d);
            if (a==b &&a==c && a ==d)
            {
                Console.WriteLine("Khong co gia tri nho nhat !");
            }
            else
            {
                Console.WriteLine($"Gia tri nho nhat cua {a} , {b} , {c} , {d} = {min3}");

            }
            */

            //vd6: nhập 4 số và in ra số lớn thứ hai
            /*
            Console.WriteLine("Nhap so nguyen a: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen b: ");
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen c: ");
            int c = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen d: ");
            int d = Convert.ToInt32(Console.ReadLine());
            int max1 = Math.Max(a, b);
            int max2 = Math.Max(max1, c);
            int max3 = Math.Min(max2, d);
            if (a == b && a == c && a == d)
            {
                Console.WriteLine("Khong co gia tri lon nhat thu hai !");
            }
            else
            {
                Console.WriteLine($"Gia tri lon nhat thu hai cua {a} , {b} , {c} , {d} = {max3}");

            }
            */

            //vd8: tính chu vi và diện tích hcn
            /*
            Console.WriteLine("Nhap chieu dai cua hinh chu nhat: ");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhap chieu rong cua hinh chu nhat: ");
            double b = Convert.ToDouble(Console.ReadLine());
            double cv = (a + b) * 2;
            double area = a * b;
            Console.WriteLine($"Chu vi cua hinh chu nhat la: {cv}");
            Console.WriteLine($"Dien tich cua hinh chu nhat la: {area}");
            */

            //vd9: giải phương trình bậc nhất
            /*
            Console.WriteLine("Nhap vao so thu nhat: ");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhap vao so thu hai: ");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"Phuong trinh bac nhat: {a}x + {b} = 0");
            if (a == 0)
            {
                if (b ==0)
                {
                    Console.WriteLine("Phuong trinh vo so nghiem !");
                }
                else
                {
                    Console.WriteLine("Phuong trinh vo nghiem !");
                }
            }
            else
            {
                Console.WriteLine($"Phuong trinh co nghiem la: x = {(-b)/a}");
            }
            */

            //vd10: giải phuong trinh bac 2
            /*
            Console.WriteLine("Nhap vao so thu nhat: ");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhap vao so thu hai: : ");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhap vao so thu ba: ");
            double c = Convert.ToDouble(Console.ReadLine());
            double delta = b * b - 4 * a * c;
            Console.WriteLine($"Phuong trinh bac hai: {a}x^2 + {b}x + {c} = 0");
            Console.WriteLine($"Delta = {delta}");
            if (a == 0)
            {
                if (b == 0)
                {
                    if (c == 0)
                    {
                        Console.WriteLine("Phuong trinh vo so nghiem !");
                    }
                    else
                    {
                        Console.WriteLine("Phuong trinh vo nghiem !");
                    }
                }
                else
                {
                    Console.WriteLine($"Phuong trinh co nghiem la: x = {(-c) / b}");
                }
            }
            else if (delta < 0)
            {
                Console.WriteLine("Phuong trinh vo nghiem ! ");
            }
            else if (delta == 0)
            {
                Console.WriteLine($"Phuong trinh co nghiem kep la: x = {(-b) / 2 * a}");
            }
            else
            {
                Console.WriteLine($"Phuong trinh co hai nghiem : x1 = {((-b) - Math.Sqrt(delta)) / 2 * a}");
                Console.WriteLine($"Phuong trinh co hai nghiem : x2 = {((-b) + Math.Sqrt(delta)) / 2 * a}");
            }
            */

            //vd11: nhập 3 số, check có phải tam giác
            /*
            Console.WriteLine("Nhap vao so thuc a: ");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhap vao so thuc b: ");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Nhap vao so thuc c: ");
            double c = Convert.ToDouble(Console.ReadLine());
            double min = Math.Min(Math.Min(a, b), c);
            double max = Math.Max(Math.Max(a, b), c);
            if (max == Math.Sqrt((min*min +(max*max-min*min) )))
            {
                Console.WriteLine($"Ba canh {a} , {b} , {c} la 3 canh cua tam giac vuong !");
            }
            */

            //vd12: 
        }
    }
}
