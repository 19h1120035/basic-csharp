﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai_02
{
    class Ex2
    {
        static void Main(string[] args)
        {
            //vd1: in số chẳn lẻ
            //Console.WriteLine("Nhap vao so nguyen n: ");
            //int n = Convert.ToInt32(Console.ReadLine());
            /*
            Console.WriteLine("Cac so chan: ");
            for (int i = 1; i <= n; i++)
            {
                if (i%2 ==0)
                {
                    Console.Write(i + "  ");
                }
            }
            Console.WriteLine("\nCac so le: ");
            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 1)
                {
                    Console.Write(i + "  ");
                }
            }
            */

            // vd2: tính tong
            /*
            Console.WriteLine("Nhap vao so nguyen n: ");
            int n = Convert.ToInt32(Console.ReadLine());
            int sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += i;
            }
            Console.WriteLine($"Tong cua 1 + ... + {n} = {sum}");
            */

            //vd3: tính tổnmg
            /*
            Console.WriteLine("Nhap vao so nguyen n: ");
            int n = Convert.ToInt32(Console.ReadLine());
            double sum = 0;
            for (int i = 1; i <= n; i++)
            {
                double tong = (double)1 / i;
                sum += tong;
            }
            Console.WriteLine(sum);
            */

            //vd4: nhập vào n, 
            /*
            Console.WriteLine("Nhap vao so nguyen n: ");
            int n = Convert.ToInt32(Console.ReadLine());
            int sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += i;
            }
            Console.WriteLine($"Tong cua 1 + ... + {n} = {sum}");
            Console.WriteLine($"Trung binh cong cua cac so tu nhien khong lon hon {n} la: {(double)sum / n}");
            double sum2 = 0;
            int count = 0;
            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 1)
                {
                    sum2 += i;
                    count++;
                }
            }
            Console.WriteLine($"Tong cua cac so le = {sum2}");
            Console.WriteLine($"Trung binh cong cua cac so le ({count}) la: {sum2 / count}");
            double sum3 = 0;
            int count3 = 0;
            for (int i = 1; i <= n; i++)
            {
                if (i % 2 == 0)
                {
                    sum3 += i;
                    count3++;
                }
            }
            Console.WriteLine($"Tong cua cac so chan = {sum3}");
            Console.WriteLine($"Trung binh cong cua cac so chan ({count3}) la: {sum3 / count3}");
            */
            //vd5: tìm giai thừa
            /*
            Console.WriteLine("Nhap vao so nguyen n: ");
            int n = Convert.ToInt32(Console.ReadLine());
            int sum = 1;
            for (int i = 1; i <=n; i++)
            {
                sum *= i; 
            }
            Console.WriteLine($"Giai thua cua {n} la : {sum}");
            */

            //vd6: nhập số tự nhiên n,k tính tổng các số <= n là chia hết cho k
            /*
            Console.WriteLine("Nhap vao so nguyen n: ");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap vao so nguyen k: ");
            int k = Convert.ToInt32(Console.ReadLine());
            int sum = 0;
            for (int i = 1; i <= n; i++)
            {
                if (i%k==0)
                {
                    sum += i;
                }
            }
            Console.WriteLine($"Tong cac so tu nhien be hon bang {n} va chia het cho {k} la: {sum}");
            */

            //vd7: nhập n và liệt kê ước số
            /*
          
            for (int i = 1; i <= n; i++)
            {
                if (n %i == 0)
                {
                    Console.Write(i +"  ");
                }
            }
            */
            //vd8: tim ucln,bcnn
            /*
            Console.WriteLine("Nhap vao so nguyen a: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap vao so nguyen b: ");
            int b = Convert.ToInt32(Console.ReadLine());
            int j = Math.Max(a,b);
            int uscln = 1;
            for (int i = 1; i <= j; i++)
            {
                if (a % i == 0 && b % i == 0)
                {
                    uscln = i;
                }
            }
            Console.WriteLine($"Uoc so chung lon nhat cua {a} va {b} la: {uscln}");
            int tamp = 1;
            for (int i = 1; i <= j; i++)
            {

                if (a % i == 0 && b % i == 0)
                {
                     tamp = i;
                }
            }
            int bscnn = (a * b) / tamp;

            Console.Write($"BSCNN cua {a} va {b} la: {bscnn}\n");
            */

            //vd9:kiểm  tra số nguyên tố
            /*
           
            int count = 0;
            for (int i = 1; i <= n; i++)
                if (n % i == 0)
                    count++;

            if (count == 2)
                Console.WriteLine($"{n} la so nguyen to !");
            else
                Console.WriteLine($"{n} khong phai la so nguyen to.");
            */
            //vd10: viết chương trình phân tích số nguyên tố thành các thừa số nguyên nguyên
            /*
            for (int i = 2; i <= n; i++)
            {
                while (n % i == 0)
                {
                    Console.Write($"{i}  ");
                    n /= i;
                }
            }
            */
            //vd11: viết chương trình liệt kê n số nguyên tố đầu tiên
            /*
            Console.Write($"{n} so nguyen to dau tien la: \n");
            int dem = 0; // dem tong so nguyen to
            int i = 2;   // tim so nguyen to bat dau tu so 2
            while (dem < n)
            {
                if (isPrimeNumber(i) == 1)
                {
                    Console.Write($"{i}  ");
                    dem++;
                }
                i++;
            }
            */

            //vd12:tìm dãy số fibonacci
            /*
            int f0 = 1;
            int f1 = 1;
            int S = 0;
            while (S <= n)
            {
                Console.Write(S+"  ");
                S = f0 + f1;
                f0 = f1;
                f1 = S;
            }
            */
            //vd14: tính n!
            /* 
            if (n == 0)
            {
                Console.WriteLine($"{n}! = 1");
            }
            int sum = 1;
            for (int i = 0; i < n; i++)
            {
                sum *= n - i;
            }
            Console.WriteLine($"{n}! = {sum}");
             */

            // vd15: viết chương trình tính tổng các chữ số trong 1 số nguyên
            /*
            int sotachra;
            int s = 0;
            while (n != 0)
            {
                sotachra = n % 10;
                s += sotachra;
                n /= 10;
            }
            Console.WriteLine($"Tong cac chu so cua n = {s}");
            */

            //vd16: in ra bảng cửu chương
            for (int i = 1; i < 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    Console.WriteLine($"{i} X {j } = {i * j}");
                }
                Console.WriteLine();
            }

        }

        private static int isPrimeNumber(int n)
        {
            if (n < 2)
            {
                return 0;
            }
            // check so nguyen to khi n >= 2
            int i;
            int squareRoot = (int)Math.Sqrt(n);
            for (i = 2; i <= squareRoot; i++)
            {
                if (n % i == 0)
                {
                    return 0;
                }
            }
            return 1;
        }
    }
}
