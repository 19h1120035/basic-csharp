﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai_02
{
    class LearnMath
    {
        static void Main(string[] args)
        {
            /*
            int max = Math.Max(10, 50);
            float min = Math.Min(1.25f, -5.234f);
            double x = -255.234;
            double d = Math.Abs(x);
            double a = Math.Sqrt(500);
            int b = (int)Math.Sqrt(500);
            Console.WriteLine($"Gia tri lon nhat cua {10} va {25} = {max}");
            Console.WriteLine($"Gia tri nho nhat cua {1.25} va {-5.234}  = {min} ");
            Console.WriteLine($"Gia tri tuyet doi cua {x} la {d}");
            Console.WriteLine($"Can bac hai cua {500} la {a}");
            Console.WriteLine($"Can bac hai cua {500} la {b}");
            */
            /*
            string msg = "                     Hello World !";
            Console.WriteLine(msg.ToUpper());
            Console.WriteLine(msg.ToLower());
            Console.WriteLine(msg.Substring(6).ToUpper());
            Console.WriteLine(msg.Trim().ToUpper());
            Console.WriteLine(msg.Contains("Hello"));
            Console.WriteLine(msg.IndexOf('o'));
            Console.WriteLine(msg.LastIndexOf('o'));
            */
            /*
            while (true)
            {
                Console.WriteLine("Vui long nhap ngay trong tuan :");
                int dayOfWeek = Convert.ToInt32(Console.ReadLine());
                switch (dayOfWeek)
                {
                    case 1:
                        Console.WriteLine("Monday");
                        break;
                    case 2:
                        Console.WriteLine("Tusday");
                        break;
                    case 3:
                        Console.WriteLine("Wendnesday");
                        break;
                    case 4:
                        Console.WriteLine("Thursday");
                        break;
                    case 5:
                        Console.WriteLine("Friday");
                        break;
                    case 6:
                        Console.WriteLine("Saturday");
                        break;
                    case 7:
                        Console.WriteLine("Sunday");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Nhap vao dung ngay trong tuan(1 - 7)");
                        Console.ForegroundColor = ConsoleColor.Black;
                        break;
                }
            }
            */
            int time = 13;
            string msg = (time < 12) ? "Hello" : "Cut";
            Console.WriteLine(msg);
        }
    }
}
