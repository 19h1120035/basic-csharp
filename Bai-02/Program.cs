﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai_02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Nhap vao ho ten cua ban: ");
            string fullName = Console.ReadLine();
            Console.WriteLine("Nhap vao so tuoi cua ban: ");
            //int age = Convert.ToInt32(Console.ReadLine());
            int age = System.Int32.Parse(Console.ReadLine());
            //float avgMark = Convert.ToSingle(Console.ReadLine());
            Console.WriteLine("Nhap diem trung binh cua ban: ");
            float avgMark = System.Single.Parse(Console.ReadLine());
            Console.WriteLine("====================================");
            Console.WriteLine("Ho va ten cua ban: " + fullName);
            Console.WriteLine("Tuoi: " + age);
            Console.WriteLine("Diem trung binh cua ban: "+avgMark);
        }
    }
}
