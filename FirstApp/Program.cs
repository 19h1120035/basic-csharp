﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "My First App In C#";
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Green;
            Console.WriteLine("*****************************");
            Console.WriteLine("***     0 0 0   0 0 0     ***");
            Console.WriteLine("***   0 0 0 0 0 0 0 0 0   ***");
            Console.WriteLine("*** 0 0 0 0 0 0 0 0 0 0 0 ***");
            Console.WriteLine("*** 0 0 0 0 0 0 0 0 0 0 0 ***");
            Console.WriteLine("***   0 0 0 0 0 0 0 0 0   ***");
            Console.WriteLine("***     0 0 0 0 0 0 0     ***");
            Console.WriteLine("***       0 0 0 0 0       ***");
            Console.WriteLine("***         0 0 0         ***");
            Console.WriteLine("***           0           ***");
            Console.WriteLine("*****************************");
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.Black;




        }
    }
}
