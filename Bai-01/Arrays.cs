﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai_01
{
    class Arrays
    {
        //static void Main() {
        //    int[] arr = { 1, 4, 56, 7, 4 };
        //    string[] name = { "Dao", "Van", "Thuong" };
        //    string[] cars = new string[100];
        //    //for (int i = 0; i < name.Length; i++)
        //    //{
        //    //    Console.Write(name[i] +" ");
        //    //}

        //    //foreach (var item in name)
        //    //{
        //    //    Console.WriteLine(item);
        //    //}
        //    //Array.Sort(arr); // sắp xếp tăng dần
        //    //Array.Reverse(arr); // đảo phân tử trong mảng
        //    //Array.Clear(arr, 1, 3); // reset các phần tử trong mảng: 1: start, 3:số phần tử
        //    int search = Array.IndexOf(name, "Van"); // vị trí trong mảng
        //    Console.WriteLine(search);
        //    //foreach (var item in arr)
        //    //{
        //    //    Console.WriteLine(item);
        //    //}

        //    // Mảnh hai chiều 
            
        //}
      
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            HocMang1Chieu();

        }
        static void HocMang1Chieu()
        {
            Console.WriteLine("Nhập vào số phần tử trong mảng: ");
            int n = int.Parse(Console.ReadLine());
            int[] m = new int[n];
            Random ran = new Random();
            for (int i = 0; i < n; i++)
            {
                m[i] = ran.Next(100);
            }
            Console.Write("Mảng ngãu nhiên là: ");
            foreach (var item in m)
            {
                Console.Write(item + " ");
            }
            Array.Reverse(m);
            Console.WriteLine();
            Console.Write("Mảng sau khi đảo chiều là : ");
            foreach (var item in m)
            {
                Console.Write(item + " ");
            }
            Array.Sort(m);
            Console.WriteLine();
            Console.Write("Mảng sau khi sắp xếp: ");
            foreach (var item in m)
            {
                Console.Write(item +" ");
            }
            Console.WriteLine();
            int sum = 0;
            foreach (var item in m)
            {
                sum += item;
            }
            Console.Write($"Tổng các phần tử trong mảng là: {sum}");
            Console.WriteLine();

            // tìm kiếm mảng
            Console.WriteLine("Mời bạn nhập vào số muốn tìm :");
            int k = int.Parse(Console.ReadLine());

            int result = Array.BinarySearch(m, k);
            if (result < 0)
            {
                Console.WriteLine($"Không tìm thấy {k} trong mảng !");
            }
            else
            {
                Console.WriteLine($"Tìm thấy {k} tại vị trí thứ {result}");
            }
            // tìm kiếm mảng khi chưa sắp xếp
            Console.WriteLine("Mời bạn nhập vào số muốn tìm khác :");
            k = int.Parse(Console.ReadLine());
            result = -1;
            int vt = 0;
            for (int i = 0; i < m.Length; i++)
            {
                if (m[i] == k)
                {
                    result = m[i];
                    vt = i;
                    break;
                }
            }
            if (result < 0)
            {
                Console.WriteLine($"Không tìm thấy {k} trong mảng !");
            }
            else
            {
                Console.WriteLine($"Tìm thấy {k} tại vị trí thứ {vt}");
            }

        }
    }
}
