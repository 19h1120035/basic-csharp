﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai_01
{
    class XuLyBietLe
    {
        static void Main()
        {
            try
            {
                Console.OutputEncoding = Encoding.UTF8;
                Console.WriteLine("Nhập vào ngày tháng năm sinh của bạn: ");
                string s = Console.ReadLine();
                DateTime birthDay = DateTime.Parse(s);
                Console.WriteLine($"Bạn vừa nhập năm sinh: {birthDay.ToString("dd/MM/yyyy")}");
                //Console.WriteLine($"Bạn vừa nhập năm sinh: {birthDay.Year}");
                //Console.WriteLine($"Bạn vừa nhập tháng sinh: {birthDay.Month}");
                //Console.WriteLine($"Bạn vừa nhập ngày sinh: {birthDay.Day}");
                HocBietLe2();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Lỗi hay không lỗi cũng vào đây !");

            }
          
        }
        static void HocBietLe2()
        {
            Console.WriteLine("Mời bạn nhập vào tử số: ");
            int tuSo = int.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập mẫu số: ");
            int mauSo = int.Parse(Console.ReadLine());
            if (mauSo == 0)
            {
                throw new ArithmeticException("Lỗi mẫu số = 0");
            }
        }

    }
}
