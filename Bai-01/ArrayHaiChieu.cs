﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bai_01
{
    class ArrayHaiChieu
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.UTF8;
            HocMangHaiChieu();

        }
        static void HocMangHaiChieu()
        {
            Console.WriteLine("Mời bạn nhập vào số dòng: ");
            int row = int.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập vào số cột: ");
            int col = int.Parse(Console.ReadLine());
            int[,] m = new int[row, col];
            Random ran = new Random();
            for (int i = 0; i < m.GetLength(0); i++)
            {
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    m[i, j] = ran.Next(100);
                }
            }
            Console.WriteLine("Mảng hai chiều là: ");
            for (int i = 0; i < m.GetLength(0); i++)
            {
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    Console.Write(m[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
